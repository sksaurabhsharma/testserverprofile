/*********************************
 * Module: PingOne Environment Selection List Filter
 *********************************
 * Various functions for the PingOne Environment Selection List Filter component.
 * Based off of selection.js
 *********************************/

// global variables
var p1EnvTooltipOpen = false;
var p1EnvTooltipConfig = [];

var NOT_FOUND_STATUS_CODE = 404;
var P1_API_ERROR_STATUS_CODE = 500;
var VALIDATION_ERROR_CODE = 422;

// This span at the end of the search results as a helper to programmatically determine when
// the search has completed.
var searchComplete = document.createElement('span');
searchComplete.id = 'search-complete';

var P1DetailsTooltip = (function () {

    function getChildVal(parentElement, childClass) {
        var child = $(parentElement).find(childClass);
        return $(child).val();
    }

    // gets all the tab config for a given page.
    function getTabConfiguration(hiddenVars) {
        var tabs = $(hiddenVars).find('.tab-config');
        var tabConfig = new Array(tabs.length);
        for (var i = 0; i < tabs.length; i++) {
            var tabTitle = getChildVal(tabs[i], '.tabTitle');
            var endPoint = getChildVal(tabs[i], '.endpoint');
            var displayName = getChildVal(tabs[i], '.displayNameField');
            var idField = getChildVal(tabs[i], '.idField');
            var queryParamConfig = $(hiddenVars).find('.query-params');
            var queryParams = new Array(queryParamConfig.length);
            for (var j = 0; j < queryParamConfig.length; j++) {
                var queryParam = {};
                var param = getChildVal(queryParamConfig[j], ".queryParam");
                queryParam[param] = getChildVal(queryParamConfig[j], ".queryValue");
                queryParams[j] = queryParam;
            }
            tabConfig[i] = {
                "endpoint": endPoint,
                "displayName": displayName,
                "idField": idField,
                "tabTitle": tabTitle,
                "queryParams": queryParams
            };
        }
        return tabConfig;
    }

    function configureConnectionList(tooltip) {
        p1EnvTooltipConfig = getTabConfiguration($(tooltip).find('.hidden-variables'));
        p1EnvTooltipConfig.sort((a, b) => (a["tabTitle"] > b["tabTitle"]) ? 1 : -1);

        var selected = $(tooltip).find('.option-container').val() ? parseInt($(tooltip).find('.option-container').val()) : 0;

        $(tooltip).find('.option-container').attr('id', 'optionList');

        var selectionList = $(tooltip).find(".option-container");
        $(selectionList).empty();

        for (var i = 0; i < p1EnvTooltipConfig.length; i++) {
            var textContent = p1EnvTooltipConfig[i]['tabTitle'];
            var option = $("<option>").val(i).text(textContent).addClass("option-tab visible");
            if (i == selected) {
                option.addClass("current");
            }
            selectionList.append(option);
        }
    }

    function getCurrentTab(element) {
        return $(element).parents('.input-selection-list').find('.current-tab')
    }

    function showSpinner(searchField) {
        var selection_data = getCurrentTab(searchField).find('.selection-data');
        // only show the spinner if its not already there.
        if ($(selection_data).find('.loader').length === 0) {
            selection_data.html('');
            addSpinner(selection_data);
        }
    }

    function addSpinner(element) {
        // only show the spinner if its not already there.
        if ($(element).find('#loader-container').length === 0) {
            $(element).append('<div id="loader-container"><div class="loader"></div></div>');
        }
    }

    function removeSpinner(parent) {
        $(parent).find('#loader-container').each(function(index, element) {
            $(element).remove();
        })
    }

    function getQueryString(tabConfig, searchValue, page) {
        var params = {
            "page": page,
            "numberPerPage": MAX_SHOWN_PER_PAGE,
            "filter": searchValue
        };

        // add in the query params defined for this tooltip.
        tabConfig['queryParams'].forEach(function (param) {
            // params = Object.assign(params, param);
            var key = Object.keys(param)[0];
            params[key] = param[key];
        });

        return params;
    }

    function getCurrentTabIndex(element) {
        var index = $(element).closest('.p1details-tooltip').find('.option-tab.current').attr("value")
        return index ? index : 0;
    }

    function displayResults(appendResultsTo, json, idField, nameField) {

       removeSpinner($(appendResultsTo).parent());

       if (json.items.length === 0) {
            displayNoResults('No results available.');
        }
        else {

            // loop through json, populating list items
            for (var i = 0; i < json.items.length && i < MAX_SHOWN_PER_PAGE; i++) {
                var item = json.items[i];
                var itemId = item[idField];
                var itemName = item[nameField];

                var childLabel = document.createElement('span');
                childLabel.className = 'list-item list-item-clickable';
                var idSpan = document.createElement('span');
                idSpan.className = 'env-id';
                var nameSpan = document.createElement('span');
                nameSpan.className = 'env-name';

                idSpan.innerHTML = itemId;
                nameSpan.innerHTML = itemName;

                // append values to each list item
                childLabel.appendChild(nameSpan);
                childLabel.appendChild(idSpan);

                // append list items
                $(appendResultsTo).append(childLabel);
                $(appendResultsTo).append(searchComplete);
            }
        }
    }

    function displayNoResults(errorMessage) {
        var itemsContainer = $('.selection-data');
        removeSpinner(itemsContainer.parent());
        itemsContainer.innerHTML = "";

        var noConnectionsResult = document.createElement('span');
        noConnectionsResult.innerHTML = errorMessage;
        noConnectionsResult.className = 'no-results';

        itemsContainer.append(noConnectionsResult);
        itemsContainer.append(searchComplete);
    }

    function retrieveData(tabConfig, searchValue, page, success) {
        $.ajax({
            url: tabConfig['endpoint'],
            contentType: 'application/json',
            data: getQueryString(tabConfig, searchValue, page),
            headers: {
                'X-XSRF-Header': 'PingFederate',
                'Authorization': 'Bearer ' + accessTokenId
            }
        })
            .done(function (data) {
                success(data);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                // stop data retrieval if receiving error code
                if (jqXHR.status == FORBIDDEN_STATUS_CODE || jqXHR.status == P1_API_ERROR_STATUS_CODE
                    || jqXHR.status == NOT_FOUND_STATUS_CODE || jqXHR.status == VALIDATION_ERROR_CODE) {
                    displayNoResults("Unable to retrieve list of environments.")
                }
                else {
                    retrieveAccessToken(function(token){
                        accessTokenId = token;
                        retrieveData(tabConfig, searchValue, page, success);
                    });
                }
            })
    }

    var page = 1;
    var noMorePages = false;

    function getMoreResults(searchField, completedCallback) {
        page++;
        var searchValue = $(searchField).val();
        var tabIndex = getCurrentTabIndex(searchField);
        var tabConfig = p1EnvTooltipConfig[tabIndex];

        var currentTab = getCurrentTab(searchField);
        var list = $(currentTab).find('.list-items-container');

        if (!noMorePages) {
            addSpinner(list);

            retrieveData(tabConfig, searchValue, page, function (results) {
                if (results.items.length === 0) {
                    noMorePages = true;
                    removeSpinner(list);
                }
                else {
                    displayResults(list[0], results, tabConfig['idField'], tabConfig['displayName']);
                }
                completedCallback();
            });
        } else {
            completedCallback();
        }
    }

    function getResults(searchField) {

        page = 1; // Reset page variable since filter has changed.
        noMorePages = false;
        var searchValue = $(searchField).val();
        var tabIndex = getCurrentTabIndex(searchField);
        var tabConfig = p1EnvTooltipConfig[tabIndex];

        if(!tabConfig) {
            displayNoResults('No PingOne Connections configured.');
        }
        else {
            showSpinner(searchField);

            retrieveData(tabConfig, searchValue, 1, function(results){

                var itemsContainer = $('.selection-data');
                removeSpinner(itemsContainer.parent());
                itemsContainer.innerHTML = "";
                var parentDiv = document.createElement('span');
                parentDiv.className = 'list-items-container';

                itemsContainer.append(parentDiv);

                displayResults(parentDiv, results, tabConfig['idField'], tabConfig['displayName']);
            });
        }

    }

    // api token
    var accessTokenId = 'fakeId';
    function retrieveAccessToken(callback) {
        $.post( '/render/pingfederate/app?service=apitoken', function(data) {
            callback(data.access_token);
        })
            .fail(function () {
                window.location.reload(true);
                return undefined;
            })
    }

    function hideTooltip() {
        $(".p1details-tooltip").removeClass('show');
        p1EnvTooltipOpen = false;
        $('.selection-container').removeClass('extra-height');
        // remove instances of id="optionList"
        $('.p1details-tooltip').find('.option-container').removeAttr('id');
    }

    return {
        init: function () {

            // toggle tooltip for specific dropdown
            $('.filter-container').on("click",function (e) {
                e.preventDefault();
                e.stopPropagation();

                var tooltipElement = $(this).parents('.p1details-tooltip');
                if (!p1EnvTooltipOpen && !tooltipElement.hasClass('disabled')) {
                    tooltipElement.addClass('show');
                    p1EnvTooltipOpen = true;

                    var selectionContainer = tooltipElement.closest('.selection-container');
                    selectionContainer.addClass('extra-height');

                    // If it is off the edge of the container, scroll horizontally to bring into view.
                    var endOfFilterPosition = tooltipElement.offset().left + 500;
                    var edgeOfContainer = selectionContainer.offset().left + selectionContainer.width();
                    var isOffRightScreen = endOfFilterPosition > edgeOfContainer;

                    var startOfFilterPosition = tooltipElement.offset().left;
                    var startOfContainer = selectionContainer.offset().left;
                    var isOffLeftScreen = startOfFilterPosition < startOfContainer;

                    if(!(isOffLeftScreen && isOffRightScreen)) {
                        var pixelsToScroll = 0;
                        if(isOffLeftScreen)
                            pixelsToScroll = startOfFilterPosition - startOfContainer;

                        if(isOffRightScreen)
                            pixelsToScroll = endOfFilterPosition - edgeOfContainer;

                        selectionContainer.animate({
                            scrollLeft: selectionContainer.scrollLeft() + pixelsToScroll
                        });
                    }

                    // show applicable number of tabs for specific dropdown
                    configureConnectionList(tooltipElement);
                    var search_input = $(tooltipElement).find('.details-search-display');
                    $(search_input).val('');

                    var contentAreas = $(tooltipElement).find('.selection-data');
                    for (var i = 0; i < contentAreas.length; i++) {
                        contentAreas[i].innerHTML = '';
                    }

                    $(search_input).focus();
                    getResults(search_input);
                }
                else {
                    // close it if the input was clicked again.
                    hideTooltip();
                }
            });

            // remove tooltip if clicked outside the box.
            $(document).on("click", function (event) {
                if ($(event.target).parents(".details-content").length === 0) {
                    hideTooltip();
                }
            });

            // allow for disabled elements on the page to hide the tooltip
            $('.disabled').on("click", function (event) {
                if ($(event.target).parents(".details-content").length === 0) {
                    hideTooltip();
                }
            });

            // choose current tab
            $('.option-container').on("change", function(e) {
                e.preventDefault();
                e.stopPropagation();

                $(this).parents('.selection-options').find('.current').removeClass('current');
                $(".option-container option:selected").addClass('current');
                var search_input = $(this).parents('.p1details-tooltip').find('.details-search-display');

                getResults(search_input);
            });

            // select an option
            $('.input-selection-list-items').on('click', '.list-item-clickable', function (e) {
                var value = $(this).find('.env-name').text();
                var id = $(this).find('.env-id').text();
                var tabIndex = getCurrentTabIndex(this);                

                $(this).parents('.p1details-tooltip').find('.details-search').val(value);
                var type = p1EnvTooltipConfig[tabIndex]['tabTitle'];
                
                $(this).parents('.p1details-tooltip').find('.details-search-id').val(type + "-" + id);
                hideTooltip();
                // for tapestry's sake, update server side
                $("#wrapperForm").submit();
            });

            // prevent the enter key from submitting the page while the selection box is open.
            $(document).on("keydown", function(e) {
                if(e.keyCode === 13 && p1EnvTooltipOpen) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            // get matching data.
            $('.details-search-display').on('input', function(event) {
                event.preventDefault();
                clearTimeout(typingTimer);
                var searchField = event.target;
                showSpinner(searchField);

                typingTimer = setTimeout(function () {
                    getResults(searchField);
                }, apiCallDelay);
            });

            var moreResultsInProgress = false;
            // get more results if reaching the bottom of the selection.
            $('.input-selection-list-items').on('scroll', function() {
                if ($(this).scrollTop() + $(this).innerHeight() >= ($(this)[0].scrollHeight - 25)) {
                    if(!moreResultsInProgress) {
                        moreResultsInProgress = true;
                        var searchField = $(this).parents('.input-selection-list').find('.details-search-display');
                        getMoreResults(searchField, function () {
                            moreResultsInProgress = false;
                        });
                    }
                }
            });
        }
    };
})();


// IE11 Will treat placeholder text as a value in the input field. So when focusing on the search field
// an API call is triggered since the "value" changed when really only the placeholder text was removed.
//
// To get around this behavior we won't have any placeholder text in IE11
function removePlaceHolderTextForIE11() {
    $(".details-search-display").each(function() {
        $(this).removeAttr("placeholder");
    })
}

$(document).ready(function () {
    if ($('.p1details-tooltip').length > 0) {
        if(navigator.userAgent.match(/Trident\/7\./)) {
            removePlaceHolderTextForIE11();
        }

        P1DetailsTooltip.init();
    }
});