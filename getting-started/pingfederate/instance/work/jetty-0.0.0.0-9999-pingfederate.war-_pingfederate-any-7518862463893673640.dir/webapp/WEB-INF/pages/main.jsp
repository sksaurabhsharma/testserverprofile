<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%@ page session="false" %>

<html xmlns="http://www.w3.org/1999/xhtml">
  <link rel="stylesheet" type="text/css" href="/pingfederate/dist/styles.css"/>
  <link href="/render/images/favicon.ico?v=4" rel="shortcut icon" type="image/x-icon"/>
  <script type="text/javascript" src="/render/scripts/v6/context-help.js"></script>

  <body>
    <div id="components-container">
      <div id="app"></div>
    </div>

    <script type="text/javascript" src="/pingfederate/dist/vendor-bundle.js"></script>
    <script type="text/javascript" src="/pingfederate/dist/polyfill-bundle.js"></script>
    <script type="text/javascript" src="/pingfederate/dist/index-bundle.js"></script>
  </body>
</html>