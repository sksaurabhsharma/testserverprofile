var Save = (function () {
  const pf_saved_key = 'pf-saved';

  function checkForSave() {
    if ($('.errortext').length === 0 && localStorage[pf_saved_key]) {
      var data = { 'eventType': 'showSaveMessage', 'message': 'Settings saved.' };
      var origin = window.origin ? window.origin : window.location.origin
      window.parent.postMessage(data, origin);
    }
    localStorage.removeItem(pf_saved_key);
  }

  return {
    init: function () {
      $(document).ready(function () {
        $('form#wrapperForm').find(':submit').on("click", function (event) {
          if (event.target && event.target.value === "Save") {
            localStorage.setItem(pf_saved_key, "true");
          }
        });
        checkForSave();
      });
    }
  };
})();

function isLocalStorageSupported() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}

if (isLocalStorageSupported()) {
  Save.init();
}